@echo off
setlocal enabledelayedexpansion

REM Check if arguments are provided
if "%~1"=="" (
    echo Please provide a directory path as an argument.
    exit /b 1
)

REM Process each provided argument
for %%f in (%*) do (
    REM Calculate the size of subdirectories
    set "folder=%%f"
    for /d %%d in ("!folder!\*") do (
        echo Processing: %%d
        for /f "tokens=3,4" %%a in ('dir /s /-c "%%d"^| find "File(s)"') do (
            echo Size of subdirectory %%d: %%a %%b
        )
    )
)

exit /b 0
